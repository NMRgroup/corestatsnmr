#' Conducting stratified random sampling
#'
#' @param data A data.frame (or data.table) to use for allocating sample
#' @param group string. The column(s) that represent strata
#' @param size number. If <1, the proportion to take from each stratum. If an
#'   integer 1+, the number of samples to take from each stratum. If size is a
#'   vector of integers, the number of samples taken for each stratum.
#'   Recommended in latter case to use a named vector
#' @param select list. Named list specifying a subset of strata to use in
#'   sampling
#' @param replace boolean. Toggling whether to sample with replacement
#' @param bothSets boolean. Toggling whether to return list of sampled and
#'   unsampled portions of data
#' @param keep.rownames For data.tables only. See ?data.table.
#'
#'  Adapted from https://gist.github.com/mrdwab/6424112 and
#'   https://gist.github.com/mrdwab/933ffeaa7a1d718bd10a
#'
#' @return A sample of the data passed to the function, optionally accounting
#'   for strata.
#' @export
#'
stratRandSample <- function(data, group, size,
                        select = NULL,
                        replace = FALSE,
                        bothSets = FALSE,
                        keep.rownames = FALSE){
  UseMethod("stratRandSample")
}

#' Stratified random sampling
#'
#' @rdname stratRandSample
#'
#' @export
#'
stratRandSample.data.frame <- function(data, group, size,
                                   select = NULL,
                                   replace = FALSE,
                                   bothSets = FALSE,
                                   keep.rownames = NULL) {
  if (is.null(select)) {
    data <- data
  } else {
    if (is.null(names(select))) stop("'select' must be a named list")
    if (!all(names(select) %in% names(data)))
      stop("Please verify your 'select' argument")
    temp <- sapply(names(select),
                   function(x) data[[x]] %in% select[[x]])
    data <- data[rowSums(temp) == length(select), ]
  }
  if(missing(group)) {
    data$dummy_var <- "dummy_group"
    group <- "dummy_var"
  }
  data_interxn <- interaction(data[group], drop = TRUE)
  data_tbl <- table(data_interxn)
  data_split <- split(data, data_interxn)
  if (length(size) > 1) {
    if (length(size) != length(data_split))
      stop("Number of groups is ", length(data_split),
           " but number of sizes supplied is ", length(size))
    if (is.null(names(size))) {
      n <- setNames(size, names(data_split))
      message(sQuote("size"), " vector entered as:\n\nsize = structure(c(",
              paste(n, collapse = ", "), "),\n.Names = c(",
              paste(shQuote(names(n)), collapse = ", "), ")) \n\n")
    } else {
      ifelse(all(names(size) %in% names(data_split)),
             n <- size[names(data_split)],
             stop("Named vector supplied with names ",
                  paste(names(size), collapse = ", "),
                  "\n but the names for the group levels are ",
                  paste(names(data_split), collapse = ", ")))
      ifelse(any(!(n < 0)),
             n <- round(n*data_tbl, digits = 0),
             paste(""))
    }
  } else if (size < 1) {
    n <- round(data_tbl * size, digits = 0)
  } else if (size >= 1) {
    if (all(data_tbl >= size) || isTRUE(replace)) {
      n <- setNames(rep(size, length.out = length(data_split)),
                    names(data_split))
    } else {
      message(
        "Some groups\n---",
        paste(names(data_tbl[data_tbl < size]), collapse = ", "),
        "---\ncontain fewer observations",
        " than desired number of samples.\n",
        "All observations have been returned from those groups.")
      n <- c(sapply(data_tbl[data_tbl >= size], function(x) x = size),
             data_tbl[data_tbl < size])
    }
  }
  temp <- lapply(
    names(data_split),
    function(x) data_split[[x]][sample(data_tbl[x],
                                     n[x], replace = replace), ])
  set1 <- do.call("rbind", temp)

  if(group == "dummy_var") {
    set1$dummy_var <- NULL
    data$dummy_var <- NULL
  }

  if (isTRUE(bothSets)) {
    set2 <- data[!rownames(data) %in% rownames(set1), ]
    list(SET1 = set1, SET2 = set2)
  } else {
    set1
  }
}

#' @rdname stratRandSample
#'
#' @export
stratRandSample.data.table <- function(data, group, size,
                                   select = NULL,
                                   replace = FALSE,
                                   bothSets = FALSE,
                                   keep.rownames = FALSE) {

  if(missing(group)) {
    data$dummy_var <- "dummy_group"
    group <- "dummy_var"
  }
  if (is.numeric(group)) group <- names(data)[group]
  if (!is.data.table(data)) data <- as.data.table(
    data, keep.rownames = keep.rownames)
  if (is.null(select)) {
    data <- data
  } else {
    if (is.null(names(select))) stop("'select' must be a named list")
    if (!all(names(select) %in% names(data)))
      stop("Please verify your 'select' argument")
    temp <- vapply(names(select), function(x)
      data[[x]] %in% select[[x]], logical(nrow(data)))
    data <- data[rowSums(temp) == length(select), ]
  }
  data_tbl <- data[, .N, by = group]
  data_tbl
  if (length(size) > 1) {
        if (length(size) != nrow(data_tbl))
      stop("Number of groups is ", nrow(data_tbl),
           " but number of sizes supplied is ", length(size))
    if (is.null(names(size))) {
      stop("size should be entered as a named vector")
    } else {
      ifelse(all(names(size) %in% do.call(
        paste, data_tbl[, group, with = FALSE])),
        n <- merge(
          data_tbl,
          setnames(data.table(names(size), ss = size),
                   c(group, "ss")), by = group),
        stop("Named vector supplied with names ",
             paste(names(size), collapse = ", "),
             "\n but the names for the group levels are ",
             do.call(paste, c(unique(
               data_tbl[, group, with = FALSE]), collapse = ", "))))
      ifelse(any(!(n < 0)),
             n <- n[, ss := round(N * ss, digits = 0)],
             paste(""))
    }
  } else if (size < 1) {
    n <- data_tbl[, ss := round(N * size, digits = 0)]
  } else if (size >= 1) {
    if (all(data_tbl$N >= size) || isTRUE(replace)) {
      n <- cbind(data_tbl, ss = size)
    } else {
      message(
        "Some groups\n---",
        do.call(paste, c(data_tbl[data_tbl$N < size][, group, with = FALSE],
                         sep = ".", collapse = ", ")),
        "---\ncontain fewer observations",
        " than desired number of samples.\n",
        "All observations have been returned from those groups.")
      n <- cbind(data_tbl, ss = pmin(data_tbl$N, size))
    }
  }
  setkeyv(data, group)
  setkeyv(n, group)
  data[, .RNID := sequence(nrow(data))]
  out1 <- data[data[n, list(
    .RNID = sample(.RNID, ss, replace)), by = .EACHI]$`.RNID`]

  if (isTRUE(bothSets)) {
    out2 <- data[!.RNID %in% out1$`.RNID`]
    data[, .RNID := NULL]
    out1[, .RNID := NULL]
    out2[, .RNID := NULL]
    list(SAMP1 = out1, SAMP2 = out2)
  } else {
    data[, .RNID := NULL]
    out1[, .RNID := NULL][]
  }
}
