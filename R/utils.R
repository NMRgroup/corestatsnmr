# General utility functions

#' One- and Two-way Weighted Freq Tables
#'
#' This is the 'wtd.table' from the 'questionr' package, copied here to reduce
#' dependencies
#'
#' @name wtd.table
#' @rdname wtd.table
#' @keywords internal
#' @export
`wtd.table` <-
    function(x,
             y = NULL,
             weights = NULL,
             digits = 3,
             normwt = FALSE,
             useNA = c("no", "ifany", "always"),
             na.rm = TRUE,
             na.show = FALSE,
             exclude = NULL)
    {
        if (is.null(weights)) {
            warning("no weights argument given, using uniform weights of 1\n")
            weights <- rep(1, length(x))
        }
        if (length(x) != length(weights)) stop("x and weights lengths must be the same")
        if (!is.null(y) & (length(x) != length(y))) stop("x and y lengths must be the same")
        miss.usena <- missing(useNA)
        useNA <- match.arg(useNA)
        weights[is.na(weights)] <- 0
        if (normwt) {
            weights <- weights * length(x)/sum(weights)
        }

        if (!missing(na.show) || !missing(na.rm)) {
            warning("'na.rm' and 'na.show' are ignored when 'useNA' is provided.\n")
        }
        if (useNA != "no" || (na.show && miss.usena)) {
            if (match(NA, exclude, nomatch = 0L)) {
                warning("'exclude' containing NA and 'useNA' != \"no\"' are a bit contradicting\n")
            }
            x <- addNA(x)
            if (!is.null(y)) y <- addNA(y)
        }
        if (useNA == "no" || (na.rm && miss.usena)) {
            s <- !is.na(x) & !is.na(weights)
            if (!is.null(y)) s <- s & !is.na(y)
            x <- x[s, drop = FALSE]
            if (!is.null(y)) y <- y[s, drop = FALSE]
            weights <- weights[s]
        }
        if (!is.null(exclude)) {
            s <- !(x %in% exclude)
            if (!is.null(y)) s <- s & !(y %in% exclude)
            x <- factor(x[s, drop = FALSE])
            if (!is.null(y)) y <- factor(y[s, drop = FALSE])
            weights <- weights[s]
        }
        if (is.null(y)) {
            result <- tapply(weights, x, sum, simplify = TRUE)
        }
        else {
            result <- tapply(weights, list(x,y), sum, simplify = TRUE)
        }
        result[is.na(result)] <- 0
        tab <- as.table(result)
        if (useNA == "ifany") {
            if (!is.null(y)) {
                if (sum(tab[,is.na(colnames(tab))]) == 0) tab <- tab[,!is.na(colnames(tab))]
                if (sum(tab[is.na(rownames(tab)),]) == 0) tab <- tab[!is.na(rownames(tab)),]
            } else {
                if (tab[is.na(names(tab))] == 0) tab <- tab[!is.na(names(tab))]
            }
        }
        tab
    }

#' Area-weighted R-value
#'
#' Returns a single area-weighted R-value from the two vectors it accepts as
#' arguments: one vector of R-values, and the other of areas associated with
#' each R-value
#'
#' @param r_val Vector of r-values
#' @param area Vector of area associated with each r-value
#'
#' @return Single area-weighted r-value
#' @export
#'
#' @examples aggRval(c(2,5,20), c(10,10,50))
aggRval <- function(r_val, area) {

    if(any(is.na(r_val))) warning("R-values contain missing values\n")
    if(any(is.na(area) | area == 0)) {
        warning("Area values contain missing values and/or zeroes\n")
    }

    pct_area <- area/sum(area)

    u_val <- 1 / r_val

    u_val[is.infinite(u_val)] <- 0

    ua <- u_val*pct_area

    ua_agg <- sum(ua)

    r_agg <- ifelse(ua_agg == 0, 0, 1/ua_agg)

    r_agg
}

#' Tidy a weighted t-test object
#'
#' @param x An htest object, such as those created by weights::wtd.t.test()
#'
#' @importFrom dplyr bind_rows
#'
#' @return A tibble::tibble() with columns for method, coefficients, estimated
#'   values, p-value, and other statistics
#' @export
#'
tidy.wtd.t.test <- function(x) {

    method <- data.frame(method = x$test)

    coef <- dplyr::bind_rows(x$coefficients)

    result <- dplyr::bind_rows(x$additional) %>%
        setNames(c("estimate", paste0("estimate", 1:2), "std.err"))

    dplyr::bind_cols(result, coef, method)
}

#' Tidy a weighted chi-squared contingency table test
#'
#' @param x An htest object, such as those created by weights::wtd.chi.sq
#'
#' @importFrom tidyr as_tibble
#'
#' @return A tibble::tibble() with columns for method, coefficients, estimated
#'   values, p-value, and other statistics
#' @export
#'
tidy.wtd.chi.sq <- function(x) {

    tidyr::as_tibble(as.list(x))

}
