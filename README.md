
<!-- README.md is generated from README.Rmd. Please edit that file -->

# coreStatsNMR

`coreStatsNMR` aims to be a set of statistical functions for use at NMR
Group when completing core analysis tasks: frequency tables, cross-tabs,
t-tests, proportion tests, etc. See a mirror of this info at
[r-universe/coreStatsNMR](https://nmrgroup.r-universe.dev/ui#package:coreStatsNMR)

## Installation

``` r
# Install coreStatsNMR in R:
install.packages("coreStatsNMR", repos = c("https://nmrgroup.r-universe.dev", "https://cran.r-project.org"))

# Browse the coreStatsNMR manual pages
help(package = 'coreStatsNMR')
```
